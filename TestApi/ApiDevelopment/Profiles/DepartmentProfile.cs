﻿using AutoMapper;

namespace ApiDevelopment.Profiles
{
    public class DepartmentProfile : Profile
    {
         public DepartmentProfile() //creating constructor to mapping the class
        {
            //define mappings
            CreateMap<Models.Department,Dtos.Department>(); 
            //use reverse map when column name is different

        }
    }
}
