﻿using ApiDevelopment.Models;

namespace ApiDevelopment.Services
{
    public interface IDepartmentServiceRepository
    {
        List<Department>GetAllDepartments();
        Department CreateDepartment(Department department);
        Department UpdateDepartment(Department department);
        Department GetDepartmentById(int departmentId);

        Department DeleteDepartment(int departmentId);
    }
}
