﻿using ApiDevelopment.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiDevelopment.Services
{
    public class DepartmentService : IDepartmentServiceRepository
    {
        private readonly ApplicationContext _appContext;
        public DepartmentService(ApplicationContext appContext)
        {
            _appContext = appContext;
        }
        public List<Department> GetAllDepartments()
        {
            if (_appContext == null)
            {
                return new List<Department>();
            }
            return _appContext.Departments.ToList();

        }

        public Department GetDepartmentById(int departmentId)
        {
            if (_appContext == null)
            {
                return new Department();
            }
            var departmentToUpdate = _appContext.Departments.FirstOrDefault(dep => dep.Id == departmentId);
            return departmentToUpdate;
        }

        public Department CreateDepartment(Department department)
        {
            _appContext.Departments.Add(department);
            _appContext.SaveChanges();
            return department;
        }

        public Department UpdateDepartment(Department department)
        {
            var existingDepartment = GetDepartmentById(department.Id);
            _appContext.Entry(existingDepartment).CurrentValues.SetValues(department);
            _appContext.SaveChanges();
            return department;
        }

        public Department DeleteDepartment(int departmentId)
        {
            var existingDepartment = GetDepartmentById(departmentId);

            if (existingDepartment != null)
            {
                _appContext.Departments.Remove(existingDepartment);
                _appContext.SaveChanges();
            }

            return existingDepartment;
        }

    }
}
