﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ApiDevelopment.Dtos
{
    public class Department
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("DepartmentID")]
        public int Id { get; set; }

        [Column("DepartmentName")]
        public string Name { get; set; }
    }
}
