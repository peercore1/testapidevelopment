﻿using ApiDevelopment.Models;
using ApiDevelopment.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiDevelopment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepartmentServiceRepository _departmentServiceRepository;
        private readonly IMapper _mapper;
        public DepartmentsController(IDepartmentServiceRepository departmentServiceRepo, IMapper mapper) //inject the mapper object here
        {
            _departmentServiceRepository = departmentServiceRepo;
            _mapper = mapper;
        }

        [HttpGet("{id}")] 
        public IActionResult GetDepartmentById(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }

            var departmentDetails = _departmentServiceRepository.GetDepartmentById(id);

            {
                var returnDepByid = _mapper.Map<Dtos.Department>(departmentDetails);
                return Ok(departmentDetails);
            }
        }

        [HttpGet]
        public IActionResult GetAllDepartments()

        {
            var allDeps = _departmentServiceRepository.GetAllDepartments().ToList();
            var returnDep = _mapper.Map<ICollection<Dtos.Department>>(allDeps);
            return Ok(allDeps);
        }

        [HttpPost]
        public IActionResult CreateDepartment([FromBody] Department department)
        {
            if (department == null)
            {
                return BadRequest("Department data is null");
            }

            var createdDepartment = _departmentServiceRepository.CreateDepartment(department);
            return CreatedAtAction("GetDepartmentById", new { id = createdDepartment.Id }, createdDepartment);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateDepartment([FromRoute] int id, [FromBody] Department department)
        {
            var existingDepartment = _departmentServiceRepository.GetDepartmentById(id);

            if (existingDepartment == null)
            {
                return NotFound();
            }
           _departmentServiceRepository.UpdateDepartment(department);
            return Ok();

        }

        [HttpDelete("{id}")]

        public IActionResult DeleteDepartment([FromRoute] int id)
        {
            var existingDepartment = _departmentServiceRepository.GetDepartmentById(id);

            if (existingDepartment == null)
            {
                return NotFound();
            }

            _departmentServiceRepository.DeleteDepartment(id);

            return NoContent();
        }

    }
}

