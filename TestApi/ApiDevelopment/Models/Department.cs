﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiDevelopment.Models
{

    [Table("Departments")]
    public class Department
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("DepartmentID")]
        public int Id { get; set; }

        [Column("DepartmentName")]
        public string Name { get; set; }
    }
}
