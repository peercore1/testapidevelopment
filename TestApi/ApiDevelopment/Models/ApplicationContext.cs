﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;

namespace ApiDevelopment.Models
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) 
        {
           
        }
        public DbSet<Department> Departments { get; set; }
    }
   
}
